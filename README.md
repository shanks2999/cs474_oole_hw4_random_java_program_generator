# README #


## CS474 Project - Code Generator ##

   Original source repository on [Github](http://code.google.com/p/json-simple/)

### Steps to execute the launcher application: ###

* Through cmd/terminal, go to project root directory.
* Run commands in sequence:
	* `gradle clean`  -> To clear the compiled gradle classes.
	* `gradle build` -> To re-build our compiled class files.
	* `gradle run` -> To run our main class from gradle.
* Generated code can be found under `org.cs474.hw4.console.generated.*`.

### Code Orgaization ###
* Main Launcher : `org.cs474.hw4.console.GeneratorRunner`
* Grammar Expanders : `org.cs474.hw4.console.treexpanders.*`
* Context / Code based Expanders : `org.cs474.hw4.console.contextExpanders.*`
* Generators : `org.cs474.hw4.console.generators.*` 
* State management : `org.cs474.hw4.console.packageState.*`
* Helper classes for state : `org.cs474.hw4.console.elements.*`
* Grammar files (hand-coded) : `/resources/*.bnf`
* Acquired code (BNF reader / parser) : `org.cs474.hw4.bnf.*`
* Configuration properties for probabilities : `resources/app.properties`

### Output ###
* Generated code : `org.cs474.hw4.console.generated.*`

### Functionalities implemented: ###
* #### Generators: ####
	* Class generator
	* Interface generator
	* Method generator
	* Statement grammar generator
	* Boolean expression grammar generator
	* Code based expanders for specific BNF tags
	