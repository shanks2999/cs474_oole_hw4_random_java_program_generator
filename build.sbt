javacOptions ++= Seq("-encoding", "UTF-8")

lazy val root = (project in file("."))
  .settings(
    Seq(
      projectDependencies ++= Seq(
        "junit" % "junit" % "4.10" % "test->default",
        "com.novocode" % "junit-interface" % "0.11" % Test
      ),
      libraryDependencies += "com.github.javaparser" % "javaparser-core" % "3.3.4",
      // https://mvnrepository.com/artifact/org.javassist/javassist
      libraryDependencies += "org.javassist" % "javassist" % "3.13.0-GA",

        crossPaths := false
    )
  )