package org.cs474.hw4.console.helpers;

import com.mifmif.common.regex.Generex;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class GenerationHelper {

    private static SecureRandom random;

    static {
        try {
            random = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static String generate(String tag) throws Exception {

        Generex generator = null;
        String lowerCaseTag = tag.toLowerCase();
        switch (lowerCaseTag) {

            case "int" :
                generator = new Generex("[1-9][0-9]{1,5}");
                break;
            case "string" :

                // generator = new Generex("\"[\\w\\W\\d]{1, 20}\"");
                generator = new Generex("[a-zA-Z0-9]*");

                // off-pattern return statement
                return "\"" + generator.random() + "\"";

            case "boolean" :
                generator = new Generex("true|false");
                break;
            case "identifier" :
                generator = new Generex("[a-zA-Z]+[a-zA-Z0-9_\\$]{7,10}");
                break;
            case "object" :
                generator = new Generex("null");
                break;
            default:
                throw new Exception("Unexpected tag : " + tag);
        }

        return generator.random();
    }

    public static boolean generateBoolean() {
        Generex generator = new Generex("true|false");
        return Boolean.parseBoolean(generator.random());
    }

    public static int random() {
        return random(10);
    }

    public static int random(int bound) {
        return random.nextInt(bound);
    }

    public static String getTypeName() {
        // TODO : implement an internal registry of initial types and add new types as and when they are added to the system
        return Arrays.asList("int", "String", "boolean", "Object").get(random(4));
    }
}
