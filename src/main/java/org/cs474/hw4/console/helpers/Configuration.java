package org.cs474.hw4.console.helpers;

import java.io.*;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Properties;

public class Configuration {

    private static Properties prop = new Properties();
    static {
        String fileName = "./resources/app.properties";

        try {

            File inputFile = Paths.get(fileName).toFile();
            InputStream is = new FileInputStream(inputFile);
            prop.load(is);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static int getValue(String property) {
        return Integer.parseInt(prop.getProperty(property));
    }

    public static boolean isApplicable(String property) {
        int configValue = Integer.parseInt(prop.getProperty(property));
        return GenerationHelper.random(100) < configValue;
    }

}
