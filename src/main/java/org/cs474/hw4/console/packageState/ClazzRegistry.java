package org.cs474.hw4.console.packageState;

import org.cs474.hw4.console.helpers.GenerationHelper;
import org.cs474.hw4.elements.AccessModifier;
import org.cs474.hw4.elements.Clazz;

import java.util.ArrayList;
import java.util.HashMap;

public class ClazzRegistry {

    private static HashMap<String, Clazz> clazzHashMap;
    static {
        clazzHashMap = new HashMap<>();
    }

    public static void put(Clazz clazz) {
        clazzHashMap.put(clazz.name, clazz);
    }

    public static Clazz get()
    {
        if(clazzHashMap.keySet().size() == 0) {
            return null;
        }

        String key = new ArrayList<String>(clazzHashMap.keySet()).get(GenerationHelper.random(clazzHashMap.keySet().size()));
        Clazz tmp = clazzHashMap.get(key);
        if(tmp.modifiers.contains(AccessModifier.FINAL))
            return null;
        else
            return tmp;
    }


}
