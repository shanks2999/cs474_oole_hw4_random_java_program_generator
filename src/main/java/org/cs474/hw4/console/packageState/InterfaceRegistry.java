package org.cs474.hw4.console.packageState;

import org.cs474.hw4.console.helpers.GenerationHelper;
import org.cs474.hw4.elements.Clazz;
import org.cs474.hw4.elements.Interface;

import java.util.ArrayList;
import java.util.HashMap;

public class InterfaceRegistry {

    private static HashMap<String, Interface> interfaceHashMap;
    static {
        interfaceHashMap = new HashMap<>();
    }

    public static void put(Interface anInterface) {
        interfaceHashMap.put(anInterface.name, anInterface);
    }

    public static Interface get()
    {
        if(interfaceHashMap.keySet().size() == 0) {
            return null;
        }

        String key = new ArrayList<String>(interfaceHashMap.keySet()).get(GenerationHelper.random(interfaceHashMap.keySet().size()));
        return interfaceHashMap.get(key);
    }


}
