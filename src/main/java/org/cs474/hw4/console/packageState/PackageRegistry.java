package org.cs474.hw4.console.packageState;

import org.cs474.hw4.console.helpers.GenerationHelper;
import org.cs474.hw4.elements.Clazz;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class PackageRegistry {

    private static HashSet<String> clazzHashMap;
    static {
        clazzHashMap = new HashSet<>();
    }

    public static void put(String packageName) {
        clazzHashMap.add(packageName);
    }

    public static String get()
    {
        if(clazzHashMap.size() == 0) {
            return null;
        }
        String key = (String)clazzHashMap.toArray()[GenerationHelper.random(clazzHashMap.toArray().length)];

        return key;
    }


}
