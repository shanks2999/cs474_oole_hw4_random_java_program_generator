package org.cs474.hw4.console.contextExpanders;

import org.cs474.hw4.console.helpers.GenerationHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ValueExpander implements IContextExpander {
    @Override
    public boolean canProcess(String tagName) {
        return tagName.equals("value");
    }

    @Override
    public List<String> getStatements(String type, String modifier) throws Exception {
        return getStatements(type, modifier, true);
    }

    @Override
    public List<String> getStatements(String type, String modifier, boolean forceCreate) throws Exception {
        return new ArrayList<>(Arrays.asList(GenerationHelper.generate(type)));
    }
}
