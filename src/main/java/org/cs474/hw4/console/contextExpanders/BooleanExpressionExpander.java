package org.cs474.hw4.console.contextExpanders;

import org.cs474.hw4.console.treexpanders.BnfExpander;
import org.cs474.hw4.console.treexpanders.ExpanderFactory;
import org.cs474.hw4.elements.Clazz;
import org.cs474.hw4.elements.Node;
import org.cs474.hw4.elements.statements.MethodDeclaration;

import java.util.List;

public class BooleanExpressionExpander implements IContextExpander {

    private BnfExpander expander;
    private Node context;
    private Node clazz;
    private MethodDeclaration method;

    public BooleanExpressionExpander(Node node) {
        this.context = node;

        if(node.getClass() == MethodDeclaration.class) {
            this.method = (MethodDeclaration)node;
            this.clazz = this.method.parent;
        } else {
            // this is a class
            this.clazz = node;
        }
        expander = ExpanderFactory.getExpander("booleanExpression", node);
    }

    @Override
    public boolean canProcess(String tagName) {
        return tagName.equals("booleanExpression");
    }

    @Override
    public List<String> getStatements(String type, String modifier) throws Exception {
        // parameters don't really matter here
        return getStatements(type, modifier, false);
    }

    @Override
    public List<String> getStatements(String type, String modifier, boolean forceCreate) throws Exception {
        return expander.expand("<booleanExpression>");
    }
}
