package org.cs474.hw4.console.contextExpanders;

import org.cs474.hw4.console.helpers.GenerationHelper;
import org.cs474.hw4.elements.AccessModifier;
import org.cs474.hw4.elements.Clazz;
import org.cs474.hw4.elements.Node;
import org.cs474.hw4.elements.Variable;
import org.cs474.hw4.elements.statements.FieldDeclaration;
import org.cs474.hw4.elements.statements.MethodDeclaration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MethodInvocationExpander implements IContextExpander {

    private Node context;
    private Node clazz;
    private MethodDeclaration method;

    public MethodInvocationExpander(Node node) {
        this.context = node;
        if(node.getClass() == MethodDeclaration.class) {
            this.method = (MethodDeclaration)node;
            this.clazz = (this.method).parent;
        } else {
            // this is a class
            this.clazz = (Clazz)node;
        }
    }

    @Override
    public boolean canProcess(String tagName) {

        return tagName.equals("methodInvocationStatement") || tagName.equals("methodInvocation");
    }

    @Override
    public List<String> getStatements(String type, String modifier) throws Exception {

        return getStatements(type, modifier, true);

    }

    @Override
    public List<String> getStatements(String type, String modifier, boolean forceCreate) throws Exception {

        if(type == null || type.isEmpty())
            type = "*";

        if(modifier == null || modifier.isEmpty())
            modifier = "*";
        if(method != null && method.modifiers.contains(AccessModifier.STATIC))
            modifier = "static";

        List<MethodDeclaration> tempList = getAllMethodDeclarations(this.clazz.methodDeclarations, type, modifier);
        if(tempList.size() > 0) {

            MethodDeclaration method = tempList.get(GenerationHelper.random(tempList.size()));
            IContextExpander identifierExpander = new IdentifierExpander(this.context);

            List<String> arguments = new ArrayList<>();
            for (Variable param :
                    method.arguments) {

                String identifierModifier = method.modifiers.contains(AccessModifier.STATIC) ? "static" : "*";
                List<String> statements = identifierExpander.getStatements(param.type, identifierModifier);

                if(statements.size() > 1) {
                    // we didn't find an appropriate field so we tried creating another one.
                    // ignore that and use a hard coded value

                    try {

                        arguments.add(GenerationHelper.generate(param.type));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    arguments.addAll(statements);
                }
            }

            String methodInvocation = method.name + "(" + String.join(",", arguments) + ")";
            return new ArrayList<>(Arrays.asList(methodInvocation));
        }

        if(forceCreate) {

            if(!type.equals("*")) {
                // if no method was found, return a value of the appropriate type
                return new ArrayList<>(Arrays.asList(GenerationHelper.generate(type)));
            } else {
                return new ArrayList<>(Arrays.asList(""));
            }

        }

        return new ArrayList<>(Arrays.asList(""));
    }

    private List<MethodDeclaration> getAllMethodDeclarations(List<MethodDeclaration> methodDeclarations, String type, String modifier) {

        List<MethodDeclaration> tempList = new ArrayList<>();

        for (MethodDeclaration methodDeclaration :
                methodDeclarations) {
            if(type.equals("*") || methodDeclaration.returnType.equals(type)) {
                for (AccessModifier accessModifier :
                        methodDeclaration.modifiers) {

                    if(modifier.equals("*") || accessModifier.name.equals(modifier)) {
                        tempList.add(methodDeclaration);
                        break;
                    }
                }
            }
        }

        return tempList;
    }

}
