package org.cs474.hw4.console.contextExpanders;

import org.cs474.hw4.elements.Clazz;

import java.util.List;

public interface IContextExpander {

    boolean canProcess(String tagName);
    List<String> getStatements(String type, String modifier) throws Exception;
    List<String> getStatements(String type, String modifier, boolean forceCreate) throws Exception;

}
