package org.cs474.hw4.console.contextExpanders;

import org.cs474.hw4.console.helpers.GenerationHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ForInitializationExpander implements IContextExpander {
    @Override
    public boolean canProcess(String tagName) {
        return tagName.equals("forInitialization");
    }

    @Override
    public List<String> getStatements(String type, String modifier) throws Exception {
        // the parameters don't matter here
        return getStatements(type, modifier, false);
    }

    @Override
    public List<String> getStatements(String type, String modifier, boolean forceCreate) throws Exception {

        String varName = GenerationHelper.generate("identifier");
        String varType = "int";

        String initializationValue = GenerationHelper.generate("int");
        String comparisonValue = GenerationHelper.generate("int");

        String comparison = "";

        if(Integer.parseInt(initializationValue) < Integer.parseInt(comparisonValue))
            comparison = Arrays.asList(" < ", " <= ").get(GenerationHelper.random(2));
        else
            comparison = Arrays.asList(" > ", " >= ").get(GenerationHelper.random(2));

        String updateOperator = comparison.contains("<") ? "++" : "--";

        String statement = varType + " " + varName + " = " + initializationValue + "; ";
        statement += varName + comparison + comparisonValue + "; ";
        statement += varName + updateOperator;

        return new ArrayList<>(Arrays.asList( statement ));
    }
}
