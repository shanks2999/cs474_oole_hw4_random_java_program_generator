package org.cs474.hw4.console.contextExpanders;

import org.cs474.hw4.console.helpers.GenerationHelper;
import org.cs474.hw4.elements.AccessModifier;
import org.cs474.hw4.elements.Node;
import org.cs474.hw4.elements.statements.FieldDeclaration;
import org.cs474.hw4.elements.statements.MethodDeclaration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VariableDeclarationExpander implements IContextExpander {

    private Node context;
    private Node clazz;
    private MethodDeclaration method;

    public VariableDeclarationExpander(Node node) {
        this.context = node;

        if(node.getClass() == MethodDeclaration.class) {
            this.method = (MethodDeclaration)node;
            this.clazz = (this.method).parent;
        } else {
            // this is a class / interface
            this.clazz = node;
        }
    }

    @Override
    public boolean canProcess(String tagName) {
        return tagName.equals("variableDeclarationStatement");
    }

    @Override
    public List<String> getStatements(String type, String modifier) throws Exception {

        return getStatements(type, modifier, false);

    }

    @Override
    public List<String> getStatements(String type, String modifier, boolean forceCreate) throws Exception {
        if(type == null || type.isEmpty())
            type = "*";

        if(modifier == null || modifier.isEmpty())
            modifier = "*";
        if(method != null && method.modifiers.contains(AccessModifier.STATIC))
            modifier = "static";

        FieldDeclaration fd = new FieldDeclaration();
        try {
            fd.variable.name = GenerationHelper.generate("identifier");

            if(type.equals("*"))
                fd.variable.type = GenerationHelper.getTypeName();
            else
                fd.variable.type = type;

            if(modifier.equals("*"))
                fd.accessModifiers.add(AccessModifier.PUBLIC);
            else {
                for (AccessModifier accessModifier :
                        AccessModifier.values()) {
                    if(accessModifier.name.equals(modifier)) {
                        fd.accessModifiers.add(accessModifier);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(method != null) {
            fd.accessModifiers = new ArrayList<>();
        }
        try {

            fd.assignment = GenerationHelper.generate(fd.variable.type);

        } catch (Exception e) {
            e.printStackTrace();
        }

        // when triggered from identifier expander
        if(forceCreate) {
            if(method != null) {
                method.fieldDeclarations.add(fd);
            } else {
                context.fieldDeclarations.add(fd);
            }
        }

        return new ArrayList<>(Arrays.asList(fd.toString()));
    }

}
