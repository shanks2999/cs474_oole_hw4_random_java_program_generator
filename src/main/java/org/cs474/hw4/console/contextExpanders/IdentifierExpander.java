package org.cs474.hw4.console.contextExpanders;

import org.cs474.hw4.console.helpers.GenerationHelper;
import org.cs474.hw4.elements.AccessModifier;
import org.cs474.hw4.elements.Clazz;
import org.cs474.hw4.elements.Node;
import org.cs474.hw4.elements.Variable;
import org.cs474.hw4.elements.statements.FieldDeclaration;
import org.cs474.hw4.elements.statements.MethodDeclaration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IdentifierExpander implements IContextExpander {

    private Node context;
    private Node clazz;
    private MethodDeclaration method;

    public IdentifierExpander(Node node) {
        this.context = node;

        if(node.getClass() == MethodDeclaration.class) {
            this.method = (MethodDeclaration)node;
            this.clazz = this.method.parent;
        } else {
            // this is a class
            this.clazz = node;
        }
    }

    @Override
    public boolean canProcess(String tagName) {
        return tagName.equals("identifier");
    }

    @Override
    public List<String> getStatements(String type, String modifier) throws Exception {

        return getStatements(type, modifier, true);

    }

    @Override
    public List<String> getStatements(String type, String modifier, boolean forceCreate) throws Exception {

        if(type == null || type.isEmpty())
            type = "*";

        if(modifier == null || modifier.isEmpty())
            modifier = "*";
        if(method != null && method.modifiers.contains(AccessModifier.STATIC))
            modifier = "static";


        List<FieldDeclaration> tempList = new ArrayList<>();
        if(this.method != null) {
            tempList.addAll(getAllFieldDeclarations(method.fieldDeclarations, type, "*"));

            List<FieldDeclaration> argsList = new ArrayList<>();
            for (Variable arg :
                    method.arguments) {

                FieldDeclaration fd = new FieldDeclaration();
                fd.variable.name = arg.name;
                fd.variable.type = arg.type;

                argsList.add(fd);
            }
            tempList.addAll(getAllFieldDeclarations(argsList, type, "*"));
        }

        if(this.context != null) {
            tempList.addAll(getAllFieldDeclarations(clazz.fieldDeclarations, type, modifier));
        }

        if(tempList.size() > 0) {
            int idx = GenerationHelper.random(tempList.size());
            return new ArrayList<>(Arrays.asList(tempList.get(idx).variable.name));
        }

        if(forceCreate) {
            // TODO : create new initialization and then return that
            IContextExpander expander = new VariableDeclarationExpander(this.context);
            // add var declaration statement to parent node declarations; ignore returned statements
            expander.getStatements(type, modifier, true);

            // now we should find a variable and this won't result in an infinite recursion. hopefully.
            //statements.addAll(getStatements(type, modifier));
        }

        return getStatements(type, modifier);
    }

    private List<FieldDeclaration> getAllFieldDeclarations(List<FieldDeclaration> fieldDeclarations, String type, String modifier) {

        List<FieldDeclaration> tempList = new ArrayList<>();

        for (FieldDeclaration fieldDeclaration :
                fieldDeclarations) {
            if(type.equals("*") || fieldDeclaration.variable.type.equals(type)) {

                if(modifier.equals("*")) {
                    tempList.add(fieldDeclaration);
                } else {

                    for (AccessModifier accessModifier :
                            fieldDeclaration.accessModifiers) {

                        if(accessModifier.name.equals(modifier)) {
                            tempList.add(fieldDeclaration);
                            break;
                        }
                    }

                }

            }
        }

        return tempList;
    }
}
