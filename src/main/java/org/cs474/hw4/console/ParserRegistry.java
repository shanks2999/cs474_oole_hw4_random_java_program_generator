package org.cs474.hw4.console;

import org.cs474.hw4.bnf.BNF;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class ParserRegistry {

    private static HashMap<String, BNF> registry;
    static {
        registry = new HashMap<>();
        registry.put("statements", readFile("resources/statements.bnf"));
        registry.put("booleanExpression", readFile("resources/booleanExpression.bnf"));
        registry.put("class", readFile("resources/class.bnf"));
    }

    private static BNF readFile(String path) {
        BNF bnfParser = new BNF();
        FileReader fileContents = null;
        BufferedReader fileContentsB = null;
        try {
            fileContents = new FileReader(path);
            fileContentsB = new BufferedReader(fileContents);
            bnfParser.read(fileContentsB);
            fileContents.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bnfParser;
    }

    public static BNF getParser(String tagName) {
        if(registry.containsKey(tagName)) {
            return registry.get(tagName);
        }

        // else
        return null;
    }
}
