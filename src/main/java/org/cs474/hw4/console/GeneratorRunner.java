package org.cs474.hw4.console;

import org.cs474.hw4.console.generators.ClassGenerator;
import org.cs474.hw4.console.generators.InterfaceGenerator;
import org.cs474.hw4.console.helpers.Configuration;
import org.cs474.hw4.console.helpers.GenerationHelper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GeneratorRunner {

    public static void main(String... args) throws Exception {

        int statementCount = Configuration.getValue("STATEMENTS_TO_GENERATE");
        final int MAX_CLASSES = statementCount/114;
        final int MAX_INTERFACES = statementCount/1368;

        List<InterfaceGenerator> interfaceGenerators = new ArrayList<>();
        List<ClassGenerator> classGenerators = new ArrayList<>();

        for (int i = 0; i < MAX_CLASSES; i++) {
            if (Configuration.isApplicable("CLASS_GENERATION_PROBABILITY")) {
                classGenerators.add(new ClassGenerator());
            }
        }

        for (int i = 0; i < MAX_INTERFACES; i++) {
            if (Configuration.isApplicable("INTERFACE_GENERATION_PROBABILITY")) {
                interfaceGenerators.add(new InterfaceGenerator());
            }
        }

        for (ClassGenerator generator :
                classGenerators) {

            String fileContents = getStatement(generator.getStatements());
            writeContents(fileContents, generator.packageName, generator.clazz.name);
        }

        for (InterfaceGenerator generator :
                interfaceGenerators) {

            String fileContents = getStatement(generator.getStatements());
            writeContents(fileContents, generator.packageName, generator.anInterface.name);
        }


    }

    private static void writeContents(String fileContents, String packageName, String fileNeme) {
        final String BASE_OUTPUT_PATH = "src/main/java";
        List<String> pathSegments = new ArrayList<>(Arrays.asList(BASE_OUTPUT_PATH.split("/")));
        pathSegments.addAll(Arrays.asList(packageName.split("\\.")));
        String[] segments = pathSegments.toArray(new String[0]);

        File outputFile = Paths.get(".", segments).toFile();
        outputFile.mkdirs();

        pathSegments.add(fileNeme + ".java");
        segments = pathSegments.toArray(new String[0]);
        outputFile = Paths.get(".", segments).toFile();
        BufferedWriter writer = null;
        try {

            writer = new BufferedWriter(new FileWriter(outputFile));
            writer.write(fileContents);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getStatement(List<String> list) {
        StringBuilder string = new StringBuilder();
        String indentation = "";

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals(";")) {


                int j = i;
                while (list.get(j).trim().equals("")) {
                    j--;
                }
                String prev = "";

                if(j >= 0)
                    prev = list.get(j);
                else
                    prev = list.get(i-1);

                if (prev.trim().equals("")
                        || prev.trim().endsWith("}")
                        || prev.trim().endsWith("{")) {
                    // do nothing
                } else {
                    if (list.size() - 1 >= i + 1 && (!list.get(i + 1).equals(";"))) {
                        string.append(indentation);
                        string.append(list.get(i) + "\n");
                    }
                }
            } else if (list.get(i).equals("{")
                    || list.get(i).endsWith("{")) {

                string.append(indentation);
                string.append(list.get(i) + "\n");

                indentation += "  ";
            } else if (list.get(i).equals("}")
                    || list.get(i).endsWith("}")) {

                string.append(indentation);
                string.append(list.get(i) + "\n");

                if (indentation.length() > 2)
                    indentation = indentation.substring(0, indentation.length() - 2);

            } else {
                //string.append(indentation);
                string.append(list.get(i) + " ");
            }
        }
        return string.toString();
    }

}
