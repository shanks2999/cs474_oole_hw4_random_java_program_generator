package org.cs474.hw4.console.treexpanders;

import org.cs474.hw4.bnf.BNF;
import org.cs474.hw4.console.ParserRegistry;
import org.cs474.hw4.elements.Node;

public class ExpanderFactory {

    public static BnfExpander getExpander(String token, Node context) {

        BnfExpander expander;
        BNF parser = ParserRegistry.getParser(token);

        switch (token) {
            case "booleanExpression":
                expander = new BnfExpander(parser, context, 10);
                parser = ParserRegistry.getParser(token);
                break;
            case "statements":
                expander = new BnfExpander(parser, context, 200);
                parser = ParserRegistry.getParser(token);
                break;

            default:
                expander = new BnfExpander(parser, context, 20);
                parser = ParserRegistry.getParser("class");
                break;
        }

        return expander;
    }

}
