package org.cs474.hw4.console.treexpanders;

import org.cs474.hw4.bnf.Token;
import org.cs474.hw4.bnf.Tree;

import java.util.List;

public interface IRuleExpander {

    List<String> expandRule(Tree<Token> root);

}
