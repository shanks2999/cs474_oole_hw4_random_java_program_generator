package org.cs474.hw4.console.treexpanders;

import org.cs474.hw4.bnf.BNF;
import org.cs474.hw4.bnf.Token;
import org.cs474.hw4.bnf.TokenType;
import org.cs474.hw4.bnf.Tree;
import org.cs474.hw4.console.contextExpanders.*;
import org.cs474.hw4.elements.AccessModifier;
import org.cs474.hw4.elements.Node;
import org.cs474.hw4.elements.statements.MethodDeclaration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BnfExpander {

    private Node context;
    private BNF bnfParser;
    private final int MAX_HOPS;
    private int iterationCounter = 0;

    public BnfExpander(BNF bnfParser, Node context) {
        this(bnfParser, context, -1);
    }

    public BnfExpander(BNF bnfParser, Node context, int maxHops) {
        this.bnfParser = bnfParser;
        this.context = context;
        MAX_HOPS = maxHops;
    }

    public void setContext(Node context) {
        this.context = context;
    }

    public List<String> expand(String term) throws Exception {

        ++iterationCounter;
//        System.out.println(iterationCounter);
//        System.out.println(term);

        List<String> strings = new ArrayList<String>();
        List<String> newStrings = new ArrayList<String>();
        Tree<Token> rule = null;

        rule = bnfParser.lookUp(term);
        if (rule == null) {
            List<String> tmp = tryContextExpanders(term);

            if(tmp == null)
                newStrings.add(term);
            else
                newStrings.addAll(tmp);

        } else {
            strings = expandTree(rule);
            for (int i = 0; i < strings.size(); i++) {
                String expander = strings.get(i);
                List<String> expanded = expand(expander);
                newStrings.addAll(expanded);
            }
        }
        return newStrings;
    }

    private List<String> expandTree(Tree<Token> tree) {
        //Random rand = new Random();
        java.security.SecureRandom rand = new java.security.SecureRandom();
        int roll;
        List<String> strings = new ArrayList<String>();
        if (tree.getValue().getType().equals(TokenType.TERMINAL)) {
            strings.add(tree.getValue().getValue());
        } else if (tree.getValue().getType().equals(TokenType.NONTERMINAL)) {
            strings.add(tree.getValue().getValue());
        } else if (tree.getValue().getType().equals(TokenType.ANYNUM)) {
            roll = rand.nextInt(5);
            if (roll > 0 && iterationCounter < MAX_HOPS) {
                for (int i = 0; i < roll; i++) {
                    for (int j = 0; j < tree.getNumberOfChildren(); j++) {
                        strings.addAll(expandTree(tree.getChild(j)));
                    }
                }
            }
        } else if (tree.getValue().getType().equals(TokenType.OPTION)) {
            roll = rand.nextInt(10);
            if (roll >= 3  && iterationCounter < MAX_HOPS) {
                for (int j = 0; j < tree.getNumberOfChildren(); j++) {
                    strings.addAll(expandTree(tree.getChild(j)));
                }
            }
        } else if (tree.getValue().getType().equals(TokenType.OR)) {

            if(iterationCounter > MAX_HOPS) {
                return strings;
            }

            roll = rand.nextInt(tree.getNumberOfChildren());
            roll = rand.nextInt(tree.getNumberOfChildren());
            strings.addAll(expandTree(tree.getChild(roll)));
        } else if (tree.getValue().getType().equals(TokenType.SEQUENCE)) {
            for (int j = 0; j < tree.getNumberOfChildren(); j++) {
                strings.addAll(expandTree(tree.getChild(j)));
            }
        } else {
            throw new RuntimeException("Invalid token found.");
        }

        return strings;
    }

    private List<String> tryContextExpanders(String term) throws Exception {

        if(!term.startsWith("<") || !term.endsWith(">")) {
            // not a valid token
            return null;
        }

        term = term.substring(1, term.length() - 1);
        String[] splitPieces = term.split("-");

        String tag = "";
        String type = "*";
        String modifier = "*";

        if(splitPieces.length >= 1)
            tag = splitPieces[0];

        if(splitPieces.length >= 2)
            type = splitPieces[1];

        if(splitPieces.length >= 3)
            modifier = splitPieces[2];

        List<String> statements = null;

        // TODO : this needs to be an iterator over a list of ContextExpanders
        List<IContextExpander> expanders = Arrays.asList(
                new IdentifierExpander(this.context),
                new VariableDeclarationExpander(this.context),
                new MethodInvocationExpander(this.context),
                new ValueExpander(),
                new BooleanExpressionExpander(this.context),
                new ForInitializationExpander()
        );

        for (IContextExpander expander :
                expanders) {
            if(expander.canProcess(tag)) {
                if(this.context.getClass() == MethodDeclaration.class
                        && this.context.modifiers.contains(AccessModifier.STATIC)) {
                    statements = expander.getStatements(type, "static");
                }
                else {
                    statements = expander.getStatements(type, modifier);
                }
            }
        }

        return statements;
    }

}
