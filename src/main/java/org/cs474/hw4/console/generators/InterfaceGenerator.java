package org.cs474.hw4.console.generators;

import org.cs474.hw4.console.helpers.Configuration;
import org.cs474.hw4.console.helpers.GenerationHelper;
import org.cs474.hw4.console.packageState.ClazzRegistry;
import org.cs474.hw4.console.packageState.InterfaceRegistry;
import org.cs474.hw4.console.packageState.PackageRegistry;
import org.cs474.hw4.elements.AccessModifier;
import org.cs474.hw4.elements.Clazz;
import org.cs474.hw4.elements.Interface;
import org.cs474.hw4.elements.Node;
import org.cs474.hw4.elements.statements.MethodDeclaration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InterfaceGenerator extends Generator {

    public String packageName;
    public List<String> imports;
    public Interface anInterface;

    private List<MethodGenerator> methodGenerators = new ArrayList<>();
    private List<InterfaceGenerator> interfaceGenerators = new ArrayList<>();

    private final String BASE_PACKAGE_NAME = "org.cs474.hw4.generated";
    private final String BASE_IMPORTS_NAME = BASE_PACKAGE_NAME + ".*";
    private final int MAX_INTERFACES_IMPLEMENTED = Configuration.getValue("MAX_INTERFACES_IMPLEMENTED");
    private final int MAX_METHODS = Configuration.getValue("MAX_METHODS_IN_CLASS");
    private final int MAX_NESTED_CLASSES = Configuration.getValue("MAX_NESTED_CLASSES");

    public InterfaceGenerator() {
        anInterface = new Interface();
    }
    public InterfaceGenerator(Interface parent) {
        this.anInterface = new Interface();
        this.anInterface.parent = parent;
    }

    @Override
    protected void initState() {
        try {

            // initialize package and imports
            if(anInterface.parent == null) {
                if(Configuration.isApplicable("NEW_PACKAGE_GENERATION_PROBABILITY")) {
                    packageName = BASE_PACKAGE_NAME + "." + GenerationHelper.generate("identifier");
                    PackageRegistry.put(packageName);
                }
                else {
                    String existingPackage = PackageRegistry.get();
                    if(existingPackage == null || existingPackage.isEmpty()) {
                        packageName = BASE_PACKAGE_NAME + "." + GenerationHelper.generate("identifier");
                        PackageRegistry.put(packageName);
                    } else {
                        packageName = existingPackage;
                    }
                }

                if(packageName == null) {
                    // no idea why this is happening
                    packageName = BASE_PACKAGE_NAME + "." + GenerationHelper.generate("identifier");
                }
                anInterface.packageName = packageName;
            }


            // set visibility
            if(anInterface.parent == null) {
                anInterface.modifiers.add(AccessModifier.PUBLIC);
            } else {
                anInterface.modifiers.add(
                        Arrays.asList(AccessModifier.PUBLIC, AccessModifier.PROTECTED, AccessModifier.PRIVATE)
                                .get(GenerationHelper.random(3))
                );
            }

            // set static-ness

            // set abstract / final -ness

            // name
            anInterface.name = GenerationHelper.generate("identifier");

            // set parent class

            // set parent Interfaces
            imports = new ArrayList<>();
            imports.add(BASE_IMPORTS_NAME);

            for (int i = 0; i< MAX_INTERFACES_IMPLEMENTED; i++)
            {
                if(Configuration.isApplicable("INTERFACE_INHERITANCE_PROBABILITY")) {
                    Interface parentInterface = InterfaceRegistry.get();

                    if(parentInterface != null && parentInterface.name != anInterface.name){
                        anInterface.implementedInterfaces.add(parentInterface);
                        anInterface.modifiers.remove(AccessModifier.PROTECTED);
                        anInterface.modifiers.remove(AccessModifier.PROTECTED);

                        Node temp = anInterface;
                        while(temp.parent != null) {
                            temp = temp.parent;
                        }
                        String currentPackage = temp.packageName;
                        try{
                            if(!currentPackage.equals(parentInterface.packageName)) {

                                // imports to be added to top level class / interface only
                                temp = anInterface;
                                while(temp.parent != null) {
                                    temp = temp.parent;
                                }
                                imports.add(parentInterface.packageName+ "." + parentInterface.name);
                            }
                        } catch (Exception ex) {

                        }
                    }
                }
            }

            if(this.methodGenerators.size() < MAX_METHODS) {

                for(int i = this.methodGenerators.size(); i < this.MAX_METHODS; i++) {
                    MethodGenerator generator = new MethodGenerator(anInterface, false);
                    this.methodGenerators.add(generator);
                }
            }

            // add generators for nested classes
            if(this.anInterface.parent == null) {
                // add nested classes for only top level classes

                for(int i=0; i < MAX_NESTED_CLASSES; i++) {
                    if(Configuration.isApplicable("NEW_NESTED_INTERFACE_PROBABILITY")) {

                        InterfaceGenerator childGen = new InterfaceGenerator(this.anInterface);
                        this.interfaceGenerators.add(childGen);

                    }
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<String> getStatements() throws Exception {
        initState();

        List<String> interfaceStatements = new ArrayList<>();

        if(this.anInterface.parent == null) {
            interfaceStatements.add("package " + packageName + ";\n");
            for (String importStatement :
                    this.imports) {
                interfaceStatements.add("import " + importStatement + ";\n");
            }
        }

        // 1. TODO : manipulate statements to insertmethod signature
        if (anInterface.modifiers.contains(AccessModifier.PUBLIC)) {
            interfaceStatements.add("public ");
        }
        if (anInterface.modifiers.contains(AccessModifier.PRIVATE)) {
            interfaceStatements.add("public ");
        }
        if (anInterface.modifiers.contains(AccessModifier.PROTECTED)) {
            interfaceStatements.add("protected ");
        }

        interfaceStatements.add("interface " + anInterface.name + " ");

        // inheritance
        if(anInterface.implementedInterfaces.size() > 0) {
            interfaceStatements.add(" extends ");

            List<String> tmpInterfaces = new ArrayList<>();
            for (Interface implementedInterface :
                    anInterface.implementedInterfaces) {

                tmpInterfaces.add(implementedInterface.name);
            }
            interfaceStatements.add(String.join(", ", tmpInterfaces));
        }

        interfaceStatements.add("{\n");

        // TODO : use bnf generator to add var declaration statements?

        // add methods
        for (MethodGenerator methodGenerator :
                this.methodGenerators) {

            interfaceStatements.addAll(methodGenerator.getStatements());
        }

        // add nested classes
        for (InterfaceGenerator childClassGenerator :
                this.interfaceGenerators) {

            interfaceStatements.addAll(childClassGenerator.getStatements());
        }

        interfaceStatements.add("\n}");

        if(this.anInterface.parent == null){
            // add only top level classes to the registry
            InterfaceRegistry.put(this.anInterface);
        }

        return interfaceStatements;
    }
}
