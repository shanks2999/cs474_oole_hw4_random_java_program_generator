package org.cs474.hw4.console.generators;

import java.util.List;

public abstract class Generator {

    protected abstract void initState() throws Exception;
    public abstract List<String> getStatements() throws Exception;

}
