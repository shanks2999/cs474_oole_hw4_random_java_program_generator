package org.cs474.hw4.console.generators;

import org.cs474.hw4.console.helpers.Configuration;
import org.cs474.hw4.console.helpers.GenerationHelper;
import org.cs474.hw4.console.treexpanders.BnfExpander;
import org.cs474.hw4.console.treexpanders.ExpanderFactory;
import org.cs474.hw4.elements.*;
import org.cs474.hw4.elements.statements.FieldDeclaration;
import org.cs474.hw4.elements.statements.MethodDeclaration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentNavigableMap;

public class MethodGenerator extends Generator {

    public Node parent;
    private MethodDeclaration methodDeclaration;
    private final int PARAMETER_COUNT = Configuration.getValue("MAX_METHOD_PARAMETER_COUNT");


    public MethodGenerator(Node node, boolean isCtor) throws Exception {

        this.parent = node;
        this.methodDeclaration = new MethodDeclaration();
        methodDeclaration.parent = node;
        methodDeclaration.name = "";
        methodDeclaration.isCtor = isCtor;
        //this.parent.methodDeclarations.add(methodDeclaration);
    }

    public MethodGenerator(Node node, MethodDeclaration parentDeclaration, boolean isCtor) {
        this.parent = node;
        methodDeclaration = copyFrom(parentDeclaration);
        methodDeclaration.parent = node;
        methodDeclaration.isCtor = isCtor;
        //this.parent.methodDeclarations.add(methodDeclaration);
    }

    private MethodDeclaration copyFrom(MethodDeclaration parentDeclaration) {

        MethodDeclaration newDeclaration = new MethodDeclaration();
        newDeclaration.name = parentDeclaration.name;

        for (Variable arg :
                parentDeclaration.arguments) {

            Variable newArg = new Variable();
            newArg.name = arg.name;
            newArg.type = arg.type;
            newDeclaration.arguments.add(arg);
        }

        for (AccessModifier modifier :
                parentDeclaration.modifiers) {

            newDeclaration.modifiers.add(modifier);
        }
        // if the method is being declared in a class and it has default modifier, remove it
        if (this.parent.getClass() == Clazz.class
                && newDeclaration.modifiers.contains(AccessModifier.DEFAULT)) {
            newDeclaration.modifiers.remove(AccessModifier.DEFAULT);
        }

        // if the method is being declared in a class consider removing abstract
        if (GenerationHelper.random(100) > 80
                && this.parent.getClass() == Clazz.class
                && newDeclaration.modifiers.contains(AccessModifier.ABSTRACT)) {
            newDeclaration.modifiers.remove(AccessModifier.ABSTRACT);
        }

        // if the method is being declared in a class you could possibly make the new method final
        if (GenerationHelper.random(100) > 80
                && this.parent.getClass() == Clazz.class
                && !newDeclaration.modifiers.contains(AccessModifier.ABSTRACT)) {
            newDeclaration.modifiers.add(AccessModifier.FINAL);
        }

        newDeclaration.returnType = parentDeclaration.returnType;

        return newDeclaration;
    }

    protected void initState() throws Exception {

        if (!methodDeclaration.name.isEmpty()) {
            // nothing to do
            return;
        }

        // else, new method
        if(methodDeclaration.isCtor)
            methodDeclaration.name = ((Clazz)parent).name;
        else
            methodDeclaration.name = GenerationHelper.generate("identifier");

        // modifiers

        // set visibility

        if (this.parent.getClass() == Clazz.class) {
            methodDeclaration.modifiers.add(
                    Arrays.asList(AccessModifier.PUBLIC, AccessModifier.PROTECTED, AccessModifier.PRIVATE)
                            .get(GenerationHelper.random(3))
            );
        } else {
            methodDeclaration.modifiers.add(AccessModifier.PUBLIC);
        }

        // set abstract / final -ness
        if (((Configuration.isApplicable("SET_INHERITED_METHOD_TO_ABSTRACT_FOR_CLASS_PROBABILITY") && this.parent.getClass() == Clazz.class)
                || (Configuration.isApplicable("SET_INHERITED_METHOD_TO_ABSTRACT_FOR_INTERFACE_PROBABILITY") && this.parent.getClass() == Interface.class)
        ) && methodDeclaration.parent.modifiers.contains(AccessModifier.ABSTRACT)
                ) {
            methodDeclaration.modifiers.remove(AccessModifier.FINAL);
            methodDeclaration.modifiers.remove(AccessModifier.PRIVATE);

            methodDeclaration.modifiers.add(AccessModifier.ABSTRACT);
        }

        if (Configuration.isApplicable("SET_INHERITED_METHOD_TO_ABSTRACT_FOR_CLASS_PROBABILITY") && this.parent.getClass() == Interface.class) {
            methodDeclaration.modifiers.add(AccessModifier.ABSTRACT);
        }

        if (Configuration.isApplicable("SET_INHERITED_METHOD_TO_ABSTRACT_FOR_INTERFACE_PROBABILITY") && this.parent.getClass() == Clazz.class
                && !methodDeclaration.modifiers.contains(AccessModifier.ABSTRACT)) {
            methodDeclaration.modifiers.add(AccessModifier.FINAL);
        }

        // set static-ness
        if (!methodDeclaration.modifiers.contains(AccessModifier.ABSTRACT)
                && Configuration.isApplicable("METHOD_STATICNESS_PROBABILITY")
                && (methodDeclaration.parent.parent != null && this.parent.getClass() == Clazz.class
                || this.parent.getClass() != Clazz.class)) {

            methodDeclaration.modifiers.add(AccessModifier.STATIC);
            methodDeclaration.modifiers.remove(AccessModifier.ABSTRACT);
        }

        if(parent.getClass() == Interface.class && methodDeclaration.modifiers.contains(AccessModifier.STATIC)) {
            methodDeclaration.modifiers.remove(AccessModifier.STATIC);
        }

        if(parent.parent != null && methodDeclaration.modifiers.contains(AccessModifier.STATIC)) {
            methodDeclaration.modifiers.remove(AccessModifier.STATIC);
        }

        // ctors cannot contain the following modifiers
        if(methodDeclaration.isCtor) {
            methodDeclaration.modifiers.remove(AccessModifier.FINAL);
            methodDeclaration.modifiers.remove(AccessModifier.PRIVATE);
            // remove this to make code compilation easier
            methodDeclaration.modifiers.remove(AccessModifier.ABSTRACT);
        }

        // parameters
        for (int i = 0; i < PARAMETER_COUNT; i++) {

            if (Configuration.isApplicable("ADD_METHOD_PARAMETER_PROBABILITY")) {
                Variable parameter = new Variable();

                parameter.type = GenerationHelper.getTypeName();
                parameter.name = GenerationHelper.generate("identifier");

                methodDeclaration.arguments.add(parameter);
            }
        }

        // return type

        if(methodDeclaration.isCtor){
            if (GenerationHelper.generateBoolean()) {
                // return type is needed
                methodDeclaration.returnType = GenerationHelper.getTypeName();
            }
        } else {
            methodDeclaration.returnType = "";
        }

    }

    protected List<String> fromGrammar() throws Exception {
        BnfExpander expander = ExpanderFactory.getExpander("statements", this.methodDeclaration);
        return expander.expand("statements");
    }

    public List<String> getStatements() throws Exception {
        initState();

        List<String> method = new ArrayList<>();
        // 1. TODO : manipulate statements to insertmethod signature
        if (methodDeclaration.modifiers.contains(AccessModifier.PUBLIC)
                && this.parent .getClass() == Clazz.class) {
            method.add("public ");
        }
        if (methodDeclaration.modifiers.contains(AccessModifier.PRIVATE)) {
            method.add("private ");
        }
        if (methodDeclaration.modifiers.contains(AccessModifier.PROTECTED)) {
            method.add("protected ");
        }

        if (methodDeclaration.modifiers.contains(AccessModifier.STATIC)) {
            method.add("static ");
        }

        if (methodDeclaration.modifiers.contains(AccessModifier.FINAL)) {
            method.add("final ");
        }
        if (methodDeclaration.modifiers.contains(AccessModifier.ABSTRACT)
                && this.parent .getClass() == Clazz.class) {
            method.add("abstract ");
        }
        if (methodDeclaration.modifiers.contains(AccessModifier.DEFAULT)) {
            method.add("default ");
        }

        if (!methodDeclaration.returnType.isEmpty()) {
            method.add(methodDeclaration.returnType + " ");
        } else {
            if(!methodDeclaration.isCtor)
                method.add("void ");
        }

        method.add(methodDeclaration.name + "(");

        List<String> tempArgs = new ArrayList<>();
        for (Variable arg :
                methodDeclaration.arguments) {

            tempArgs.add(arg.type + " " + arg.name);
        }
        method.add(String.join(",", tempArgs));

        if (methodDeclaration.modifiers.contains(AccessModifier.ABSTRACT)
                || (methodDeclaration.parent.getClass() == Interface.class
                    && !methodDeclaration.modifiers.contains(AccessModifier.DEFAULT))
                ) {
            method.add(");");
            return method;
        }

        // else, non-abstract method, has a body
        method.add(") {\n");


        // 2.
        List<String> contents = fromGrammar();

        // add variables
        for (FieldDeclaration fd :
                methodDeclaration.fieldDeclarations) {
            method.add(fd.toString());
        }

        method.addAll(contents);

        if (!methodDeclaration.returnType.isEmpty()) {
            method.add("return " + GenerationHelper.generate(methodDeclaration.returnType) + ";");
        }

        // 3.
        method.add("\n}\n\n");

        // 4.
        return method;
    }
}
