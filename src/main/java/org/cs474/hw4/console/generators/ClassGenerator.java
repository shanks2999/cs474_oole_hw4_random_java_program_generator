package org.cs474.hw4.console.generators;

import org.cs474.hw4.console.helpers.Configuration;
import org.cs474.hw4.console.packageState.ClazzRegistry;
import org.cs474.hw4.console.packageState.InterfaceRegistry;
import org.cs474.hw4.console.helpers.GenerationHelper;
import org.cs474.hw4.console.packageState.PackageRegistry;
import org.cs474.hw4.elements.*;
import org.cs474.hw4.elements.statements.FieldDeclaration;
import org.cs474.hw4.elements.statements.MethodDeclaration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClassGenerator extends Generator {

    public String packageName;
    public List<String> imports;
    public Clazz clazz;

    private List<MethodGenerator> methodGenerators = new ArrayList<>();
    private List<MethodGenerator> constructorGenerators = new ArrayList<>();
    private List<ClassGenerator> classGenerators = new ArrayList<>();

    private final String BASE_PACKAGE_NAME = "org.cs474.hw4.generated";
    private final String BASE_IMPORTS_NAME = BASE_PACKAGE_NAME + ".*";
    private final int MAX_INTERFACES_IMPLEMENTED = Configuration.getValue("MAX_INTERFACES_IMPLEMENTED");
    private final int MAX_METHODS = Configuration.getValue("MAX_METHODS_IN_CLASS");
    private final int MAX_NESTED_CLASSES = Configuration.getValue("MAX_NESTED_CLASSES");
    private final int MAX_FIELD_DECLARATIONS = Configuration.getValue("MAX_FIELD_DECLARATIONS");
    private final int MAX_CTOR_DECLARATIONS = Configuration.getValue("MAX_CTOR_DECLARATIONS");

    public ClassGenerator() {
        clazz = new Clazz();
    }

    public ClassGenerator(Clazz parent) {
        this.clazz = new Clazz();
        this.clazz.parent = parent;
    }

    @Override
    protected void initState() {
        try {

            // initialize package and imports
            if (clazz.parent == null) {

                if (Configuration.isApplicable("NEW_PACKAGE_GENERATION_PROBABILITY")) {
                    packageName = BASE_PACKAGE_NAME + "." + GenerationHelper.generate("identifier");
                    PackageRegistry.put(packageName);
                } else {
                    String existingPackage = PackageRegistry.get();
                    if (existingPackage == null || existingPackage.isEmpty()) {
                        packageName = BASE_PACKAGE_NAME + "." + GenerationHelper.generate("identifier");
                        PackageRegistry.put(packageName);
                    } else {
                        packageName = existingPackage;
                    }
                }

                if (packageName == null) {
                    // no idea why this is happening
                    packageName = BASE_PACKAGE_NAME + "." + GenerationHelper.generate("identifier");
                }
                clazz.packageName = packageName;
            }

            // set visibility
            if (clazz.parent == null) {
                clazz.modifiers.add(AccessModifier.PUBLIC);
            } else {
                clazz.modifiers.add(
                        Arrays.asList(AccessModifier.PUBLIC, AccessModifier.PROTECTED, AccessModifier.PRIVATE)
                                .get(GenerationHelper.random(3))
                );
            }

            // set static-ness
            if (clazz.parent != null && GenerationHelper.generateBoolean()) {
                clazz.modifiers.add(AccessModifier.STATIC);
            }

            // set abstract / final -ness
            if (GenerationHelper.generateBoolean()) {
                clazz.modifiers.add(Arrays.asList(AccessModifier.ABSTRACT, AccessModifier.FINAL)
                        .get(GenerationHelper.random(2)));
            }

            // name
            clazz.name = GenerationHelper.generate("identifier");

            // parameters
            for (int i = 0; i < MAX_FIELD_DECLARATIONS; i++) {

                if (Configuration.isApplicable("NEW_FIELD_DECLARATION_PROBABILITY")) {
                    Variable parameter = new Variable();

                    parameter.type = GenerationHelper.getTypeName();
                    parameter.name = GenerationHelper.generate("identifier");

                    FieldDeclaration fd = new FieldDeclaration();
                    fd.variable = parameter;

                    if (Configuration.isApplicable("NEW_FIELD_ASSIGNMENT_PROBABILITY")) {
                        fd.assignment = GenerationHelper.generate(parameter.type);
                    }
                    clazz.fieldDeclarations.add(fd);
                }
            }

            imports = new ArrayList<>();
            imports.add(BASE_IMPORTS_NAME);

            // set parent class
            if (Configuration.isApplicable("CLASS_INHERITANCE_PROBABILITY")) {
                clazz.extendedClazz = ClazzRegistry.get();

                // do not extend final classes
                if (clazz.extendedClazz != null && clazz.extendedClazz.fieldDeclarations.contains(AccessModifier.FINAL)) {
                    clazz.extendedClazz = null;
                }
                // class  cannot extend itself
                if (clazz.extendedClazz != null && clazz.extendedClazz.name.equals(clazz.name)) {
                    clazz.extendedClazz = null;
                }

                // add import
                Node temp = clazz;
                while (temp.parent != null) {
                    temp = temp.parent;
                }
                String currentPackage = temp.packageName;
                try {
                    if (clazz.extendedClazz != null && !currentPackage.equals(clazz.extendedClazz.packageName)) {
                        // imports to be added to top level class / interface only
                        temp = clazz;
                        while (temp.parent != null) {
                            temp = temp.parent;
                        }
                        imports.add(clazz.packageName + "." + clazz.extendedClazz.name);
                    }
                } catch (Exception ex) {

                }
            }

            // set parent Interfaces
            for (int i = 0; i < MAX_INTERFACES_IMPLEMENTED; i++) {
                if (Configuration.isApplicable("INTERFACE_INHERITANCE_PROBABILITY")) {
                    Interface parentInterface = InterfaceRegistry.get();

                    if (parentInterface != null) {
                        clazz.implementedInterfaces.add(parentInterface);
                        Node temp = clazz;
                        while (temp.parent != null) {
                            temp = temp.parent;
                        }
                        String currentPackage = temp.packageName;
                        try {
                            if (!currentPackage.equals(parentInterface.packageName)) {

                                // imports to be added to top level class / interface only
                                temp = clazz;
                                while (temp.parent != null) {
                                    temp = temp.parent;
                                }
                                imports.add(parentInterface.packageName + "." + parentInterface.name);
                            }
                        } catch (Exception ex) {

                        }
                    }
                }
            }

            // initialize required methods from inherited class
            if (clazz.extendedClazz != null) {
                for (MethodDeclaration methodDeclaration :
                        clazz.extendedClazz.methodDeclarations) {

                    if (methodDeclaration.modifiers.contains(AccessModifier.ABSTRACT)
                            && !methodDeclaration.modifiers.contains(AccessModifier.FINAL)
                            && (methodDeclaration.modifiers.contains(AccessModifier.PUBLIC)
                            || methodDeclaration.modifiers.contains(AccessModifier.PROTECTED)
                            || methodDeclaration.modifiers.contains(AccessModifier.ABSTRACT)
                    )
                            ) {

                        MethodGenerator generator = new MethodGenerator(clazz, methodDeclaration, false);
                        this.methodGenerators.add(generator);
                    }
                }

                for (MethodDeclaration ctorDeclaration :
                        clazz.constructorDeclarations) {
                    MethodGenerator generator = new MethodGenerator(clazz, ctorDeclaration, true);
                    this.constructorGenerators.add(generator);
                }

            }

            // initialize required methods from implemented interfaces
            if (clazz.implementedInterfaces != null) {

                for (Interface anInterface :
                        clazz.implementedInterfaces) {

                    for (MethodDeclaration methodDeclaration :
                            anInterface.getAllMethodDeclarations()) {

                        if (methodDeclaration.modifiers.contains(AccessModifier.ABSTRACT)) {
                            MethodGenerator generator = new MethodGenerator(clazz, methodDeclaration, false);
                            this.methodGenerators.add(generator);
                        }
                    }
                }
            }

            // consider overriding some methods from parent class
            if (clazz.extendedClazz != null) {
                if (Configuration.isApplicable("METHOD_OVERRIDE_PROBABILITY")) {
                    for (MethodDeclaration methodDeclaration :
                            clazz.extendedClazz.methodDeclarations) {

                        if ((!methodDeclaration.modifiers.contains(AccessModifier.ABSTRACT)
                                && !methodDeclaration.modifiers.contains(AccessModifier.FINAL)
                                && (methodDeclaration.modifiers.contains(AccessModifier.PUBLIC)
                                || methodDeclaration.modifiers.contains(AccessModifier.PROTECTED)
                        )
                                || methodDeclaration.modifiers.contains(AccessModifier.ABSTRACT))
                                ) {

                            MethodGenerator generator = new MethodGenerator(clazz, methodDeclaration, false);
                            this.methodGenerators.add(generator);
                        }
                    }
                }
            }

            if (clazz.extendedClazz == null && this.constructorGenerators.size() < MAX_CTOR_DECLARATIONS) {
                for (int i = this.constructorGenerators.size(); i < this.MAX_CTOR_DECLARATIONS; i++) {
                    if (Configuration.isApplicable("NEW_CTOR_PROBABILITY")) {
                        MethodGenerator generator = new MethodGenerator(clazz, true);
                        this.constructorGenerators.add(generator);
                    }
                }
            }

            if (this.methodGenerators.size() < MAX_METHODS) {

                for (int i = this.methodGenerators.size(); i < this.MAX_METHODS; i++) {
                    MethodGenerator generator = new MethodGenerator(clazz, false);
                    this.methodGenerators.add(generator);
                }
            }

            // add generators for nested classes
            if (this.clazz.parent == null) {
                // add nested classes for only top level classes
                for (int i = 0; i < MAX_NESTED_CLASSES; i++) {
                    if (Configuration.isApplicable("NEW_NESTED_CLASS_PROBABILITY")) {

                        ClassGenerator childGen = new ClassGenerator(this.clazz);
                        this.classGenerators.add(childGen);

                    }
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<String> getStatements() throws Exception {
        initState();

        List<String> classStatements = new ArrayList<>();

        if (this.clazz.parent == null) {
            classStatements.add("package " + packageName + ";\n");
            for (String importStatement :
                    this.imports) {
                classStatements.add("import " + importStatement + ";\n");
            }
        }

        // 1. TODO : manipulate statements to insertmethod signature
        if (clazz.modifiers.contains(AccessModifier.PUBLIC)) {
            classStatements.add("public ");
        }
        if (clazz.modifiers.contains(AccessModifier.PRIVATE)) {
            classStatements.add("public ");
        }
        if (clazz.modifiers.contains(AccessModifier.PROTECTED)) {
            classStatements.add("protected ");
        }

        if (clazz.modifiers.contains(AccessModifier.STATIC)) {
            classStatements.add("static ");
        }

        if (clazz.modifiers.contains(AccessModifier.FINAL)) {
            classStatements.add("final ");
        }
        if (clazz.modifiers.contains(AccessModifier.ABSTRACT)) {
            classStatements.add("abstract ");
        }

        classStatements.add("class " + clazz.name + " ");

        // inheritance
        if (clazz.extendedClazz != null) {
            classStatements.add(" extends " + clazz.extendedClazz.name + " ");
        }
        if (clazz.implementedInterfaces.size() > 0) {
            classStatements.add(" implements ");

            List<String> tmpInterfaces = new ArrayList<>();
            for (Interface implementedInterface :
                    clazz.implementedInterfaces) {

                tmpInterfaces.add(implementedInterface.name);
            }
            classStatements.add(String.join(", ", tmpInterfaces));
        }

        classStatements.add("{\n");

        // TODO : use bnf generator to add var declaration statements?

        // add variables
        for (FieldDeclaration fd :
                clazz.fieldDeclarations) {
            classStatements.add(fd.toString());
        }

        // add ctors
        for (MethodGenerator ctorGenerator :
                constructorGenerators) {
            classStatements.addAll(ctorGenerator.getStatements());
        }

        // add methods
        for (MethodGenerator methodGenerator :
                this.methodGenerators) {

            classStatements.addAll(methodGenerator.getStatements());
        }

        // add nested classes
        for (ClassGenerator childClassGenerator :
                this.classGenerators) {

            classStatements.addAll(childClassGenerator.getStatements());
        }

        classStatements.add("\n}");

        if (this.clazz.parent == null) {
            // add only top level classes to the registry
            ClazzRegistry.put(this.clazz);
        }

        return classStatements;
    }
}
