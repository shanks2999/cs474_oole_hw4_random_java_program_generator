package org.cs474.hw4.elements;

import org.cs474.hw4.elements.statements.FieldDeclaration;
import org.cs474.hw4.elements.statements.MethodDeclaration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public abstract class Node {

    public String packageName;
    public Node parent;
    public HashSet<AccessModifier> modifiers;
    public List<FieldDeclaration> fieldDeclarations;
    public List<MethodDeclaration> methodDeclarations;

    public Node() {
        this.modifiers = new HashSet<>();
        this.fieldDeclarations = new ArrayList<>();
        this.methodDeclarations = new ArrayList<>();
    }

    public abstract String toString();

}
