package org.cs474.hw4.elements;

public class Variable extends Node{
    public String type;
    public String name;

    @Override
    public String toString() {
        return type + " " + name;
    }
}
