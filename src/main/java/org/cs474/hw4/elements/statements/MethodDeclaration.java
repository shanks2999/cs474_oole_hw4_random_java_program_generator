package org.cs474.hw4.elements.statements;

import org.cs474.hw4.elements.AccessModifier;
import org.cs474.hw4.elements.Node;
import org.cs474.hw4.elements.Variable;

import java.util.*;

public class MethodDeclaration extends Statement {
    public Node parent;

    public HashSet<AccessModifier> modifiers;
    public boolean isStatic;
    public boolean isAbstract;
    public String returnType;


    public String name;
    public  List<Variable> arguments;
    public boolean isCtor;

    public MethodDeclaration() {
        returnType = "";
        modifiers = new HashSet<>();
        arguments = new ArrayList<>();
        fieldDeclarations = new ArrayList<>();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        List<String> modifiers = new ArrayList<>();
        this.modifiers.forEach((item) -> modifiers.add(item.toString()));

        sb.append(String.join(" ", modifiers));
        if (this.isAbstract)
            sb.append(" abstract");

        if (this.isStatic)
            sb.append(" static");

        sb.append(returnType)
                .append(name)
                .append("(");

        for (int i = 0; i < arguments.size() - 1; i++) {
            sb.append(arguments.get(i).toString());

            if (i < arguments.size() - 1)
                sb.append(",");
        }

        return sb.toString();
    }


}
