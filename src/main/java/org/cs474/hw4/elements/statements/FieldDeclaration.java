package org.cs474.hw4.elements.statements;

import org.cs474.hw4.elements.AccessModifier;
import org.cs474.hw4.elements.Variable;

import java.util.ArrayList;
import java.util.List;

public class FieldDeclaration extends Statement {

    public FieldDeclaration() {
        this.variable = new Variable();
        accessModifiers = new ArrayList<>();
        assignment = "";
    }

    public Variable variable;
    public List<AccessModifier> accessModifiers;
    public String assignment;


    @Override
    public String toString() {
        List<String> modifiers = new ArrayList<>();

        for (AccessModifier accessModifier :
                accessModifiers) {
            modifiers.add(accessModifier.name);
        }

        return String.join(" ", modifiers) + " " + variable.toString() + (assignment.isEmpty() ? ";" :  " = " + assignment + ";");
    }
}
