package org.cs474.hw4.elements;


import org.cs474.hw4.elements.statements.MethodDeclaration;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Clazz extends Node {

    public String name;
    public Clazz extendedClazz;
    public HashSet<Interface> implementedInterfaces;

    public List<Interface> interfaces;
    public List<Clazz> classes;
    public List<MethodDeclaration> constructorDeclarations;

    public Clazz() {

        modifiers = new HashSet();

        parent = null;
        extendedClazz = null;

        implementedInterfaces = new HashSet<>();
        interfaces = new ArrayList<>();
        classes = new ArrayList<>();
        fieldDeclarations = new ArrayList<>();
        methodDeclarations = new ArrayList<>();
        constructorDeclarations = new ArrayList<>();
    }

    @Override
    public String toString() {
        return this.name;
    }

/*    public List<FieldDeclaration> getPublicFields() {
        List<FieldDeclaration> publicFields = new ArrayList<>();
        for (FieldDeclaration declaration :
                fieldDeclarations) {
            if(declaration.accessModifiers.contains(AccessModifier.PUBLIC)) {
                publicFields.add(declaration);
            }
        }

        return publicFields;
    }

    public List<FieldDeclaration> getQualifiedFields(List<AccessModifier> modifiers) {
        return getQualifiedFields(modifiers, "");
    }

    public List<FieldDeclaration> getQualifiedFields(List<AccessModifier> modifiers, String type) {
        List<FieldDeclaration> fields = new ArrayList<>();
        for (FieldDeclaration declaration :
                fieldDeclarations) {

            if (declaration.accessModifiers.contains(modifiers)) {
                if (type == null || type.isEmpty() || declaration.variable.type.equals(type)) {
                    fields.add(declaration);
                }
            }
        }

        return fields;
    }

    public List<MethodDeclaration> getMethodDeclarations(List<AccessModifier> modifiers) {
        return getMethodDeclarations(modifiers, "");
    }

    public List<MethodDeclaration> getMethodDeclarations(List<AccessModifier> modifiers, String type) {
        List<MethodDeclaration> methods = new ArrayList<>();

        for (MethodDeclaration method :
                methodDeclarations) {
            if (method.modifiers.contains(modifiers)) {
                if(type == null || type.isEmpty() || method.returnType.equals(type)) {
                    methods.add(method);
                }
            }
        }
        
        return methods;
    }*/

}
