package org.cs474.hw4.elements;

public enum AccessModifier {
    PUBLIC("public"),
    PRIVATE("private"),
    PROTECTED("protected"),
    FINAL("final"),
    ABSTRACT("abstract"),
    STATIC("static"),
    DEFAULT("default");

    public final String name;

    private AccessModifier(String name){
        this.name = name;
    }
}
