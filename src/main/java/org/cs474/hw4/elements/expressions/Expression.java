package org.cs474.hw4.elements.expressions;

import org.cs474.hw4.elements.Node;

public abstract class Expression extends Node {

    public abstract String toString();

}
