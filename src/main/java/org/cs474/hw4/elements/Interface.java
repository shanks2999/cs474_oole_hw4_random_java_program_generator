package org.cs474.hw4.elements;

import org.cs474.hw4.elements.statements.MethodDeclaration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Interface extends Node {

    public String name;
    public HashSet<Interface> implementedInterfaces;

    public Interface() {
        this(null);
    }

    public Interface(Node parent) {
        this.parent = parent;

        modifiers = new HashSet<>();
        implementedInterfaces = new HashSet<>();
        fieldDeclarations = new ArrayList<>();
        methodDeclarations = new ArrayList<>();
    }

    @Override
    public String toString() {
        return this.name;
    }

    public List<MethodDeclaration> getAllMethodDeclarations() {

        List<MethodDeclaration> tempMethodList = new ArrayList<>();
        for (MethodDeclaration methodDeclaration:
             this.methodDeclarations) {
            tempMethodList.add(methodDeclaration);
        }
        for (Interface anInterface :
                this.implementedInterfaces) {
            tempMethodList.addAll(anInterface.getAllMethodDeclarations());
        }

        return tempMethodList;
    }
}
